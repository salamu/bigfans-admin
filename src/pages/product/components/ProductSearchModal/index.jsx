import React from 'react';
import {Form, Input, Breadcrumb, Card, Cascader, Select, Modal, DatePicker , Table, Button, Col , InputNumber} from 'antd';
import SearchForm from '../../list/SearchForm'
import SearchList from '../../list/SearchList'

class ProductSearchModal extends React.Component {

	state={
		loading : false,
		selectedRows : [],
		data : [{
			  key: '1',
			  name: 'John Brown',
			  age: 32,
			  address: 'New York No. 1 Lake Park',
			}, {
			  key: '2',
			  name: 'Jim Green',
			  age: 42,
			  address: 'London No. 1 Lake Park',
			}, {
			  key: '3',
			  name: 'Joe Black',
			  age: 32,
			  address: 'Sidney No. 1 Lake Park',
			}, {
			  key: '4',
			  name: 'Disabled User',
			  age: 99,
			  address: 'Sidney No. 1 Lake Park',
			}]
	}

	changeLoading = (loading) => {
		this.setState({loading})
	}

	receiveData = (data) => {
		this.setState({data})
	}

	onSelectChange = (selectedRowKeys, selectedRows) => {
		this.setState({selectedRows})
	}

	render () {
		return (
				<Modal
				  key={this.props.key}
                  title="选择商品"
                  maskClosable={false}
                  closable
                  width="70%"
                  visible={this.props.visible}
                  onOk={() => {this.props.onOk(this.state.selectedRows)}}
                  onCancel={this.props.onCancel}
                >
                    <SearchForm changeLoading={this.changeLoading} receiveData={this.receiveData}/>
                    <SearchList onSelectChange={this.onSelectChange} 
                    			showCheckbox={true} 
                    			hideAction={true}
                    			dataSource={this.state.data} 
                    			loading={this.state.loading} />
                </Modal>
			)
	}
}

export default ProductSearchModal;