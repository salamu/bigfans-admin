import React from 'react';
import {Form, Button} from 'antd';
import AttributeWall from 'components/AttributeWall'
const FormItem = Form.Item;

/**
 *
 */
class ProductCreateStep3 extends React.Component {

    constructor(props){
        super(props);
    }

    state = {
        confirmDirty: false,
        autoCompleteResult: [],
    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    }

    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 20},
                sm: {span: 3},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 14},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 14,
                    offset: 6,
                },
            },
        };

        return (
            <div style={this.props.style}>
                <AttributeWall form={this.props.form} attributes={this.props.attributes}/>
            </div>
        );
    }
}

export default ProductCreateStep3;