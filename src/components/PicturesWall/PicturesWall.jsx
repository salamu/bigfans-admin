import React from 'react';
import { Upload, Icon, Modal ,Form,Input} from 'antd';
import StorageUtils from 'utils/StorageUtils'

class PicturesWall extends React.Component {

    constructor(props){
        super(props);
        this.maxsize = this.props.maxsize || 3;
    }

    state = {
        previewVisible: false,
        previewImage: '',
        fileList: [],
    };

    handleCancel = () => this.setState({ previewVisible: false })

    handlePreview = (file) => {
        this.setState({
            previewImage: file.url || file.thumbUrl,
            previewVisible: true,
        });
    }

    getUploadUrl(){

    }

    handleChange = ({ file,fileList }) => {
        if(file.status==='done' && file.response){
            this.onUploaded(file);
        }
        this.setState({ fileList });
    }

    onUploaded (file) {

    }

    handleRemove = (file) => {

    }

    render() {
        const { previewVisible, previewImage, fileList } = this.state;
        const uploadButton = (
            <div>
                <Icon type="plus" />
                <div className="ant-upload-text">Upload</div>
            </div>
        );
        const headers = {}
        let token = StorageUtils.getToken()
        if(token){
            headers.Authorization = 'Bearer ' + token
        }
        return (
            <div className="clearfix">
                <Upload
                    action={this.getUploadUrl()}
                    headers={headers}
                    name="file"
                    listType="picture-card"
                    fileList={fileList}
                    onPreview={this.handlePreview}
                    onChange={this.handleChange}
                    onRemove={this.handleRemove}
                >
                    {fileList.length >= this.maxsize ? null : uploadButton}
                    {
                        this.props.form 
                        &&
                        this.renderFormField({fileList})
                    }
                </Upload>
                <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
                    <img alt="example" style={{ width: '100%' }} src={previewImage} />
                </Modal>
            </div>
        );
    }

    renderFormField (fileList) {

    }
}

export default PicturesWall;