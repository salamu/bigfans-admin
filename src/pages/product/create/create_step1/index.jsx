import React from 'react';
import {Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete , InputNumber , message} from 'antd'
import SearchInput from 'components/SearchInput/SearchInput'
import TagGroup from 'components/TagGroup/TagGroup'
import BaseComponent from 'components/BaseComponent'
import IntroductionEditor from '../../components/ProductIntroductionEditor'
import BrandSearchInput from '../../components/BrandSearchInput'
import ProductTags from '../../components/ProductTags'
import AppHelper from 'utils/AppHelper';
import HttpUtils from 'utils/HttpUtils'
const FormItem = Form.Item;
const {TextArea} = Input


const formItemLayout = {
    labelCol: {
        xs: {span: 20},
        sm: {span: 3},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 14},
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 14,
            offset: 6,
        },
    },
};

class ProductCreatePage extends BaseComponent {

    constructor(props){
        super(props);
    }

    state = {
        confirmDirty: false,
        autoCompleteResult: [],
        categories : []
    };

    componentDidMount () {
        let self = this;
        HttpUtils.listCategories({
            success(resp){
                let categories = AppHelper.tools.formatCategories(resp.data , false);
                self.setState({categories});
            },
            error(resp){
                message.error(resp.error_message)
            }
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                const path = `#/app/product/create_step2`;
            }
        });
    }

    receiveHtml = (content) => {
        console.info("content = " + content)
    }

    onIntroductionChange(info){
        console.log("onChange:",info);
    }

    onCategoryChange = (value) => {
        this.props.onCategoryChange(value);
    }

    render() {

        const {getFieldDecorator} = this.props.form;


        return (
            <div style={this.props.style}>
                <FormItem {...formItemLayout} label="商品类别">
                    {getFieldDecorator('productGroup.categoryId', {
                        initialValue: [],
                        rules: [{
                            type: 'array',
                            required: true,
                            message: '请选择商品类别!'
                        }],
                    })(
                        <Cascader placeholder="请选择商品类别" 
                                  options={this.state.categories} 
                                  changeOnSelect 
                                  onChange={this.props.onCategoryChange}/>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="商品名称">
                    {getFieldDecorator('productGroup.name', {
                        rules: [{required: true, message: 'Please input your nickname!', whitespace: true}],
                    })(
                        <Input />
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="产地">
                    {getFieldDecorator('productGroup.origin')(
                        <Input />
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="品牌">
                    {getFieldDecorator('productGroup.brandId')(
                        <BrandSearchInput form={this.props.form}/>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="标签">
                    {getFieldDecorator('productGroup.tags')(
                        <ProductTags form={this.props.form}/>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="商品介绍">
                    {getFieldDecorator('productGroup.description', {
                        rules: [{message: '请填写商品介绍!', whitespace: true}],
                    })(
                        <IntroductionEditor form={this.props.form}/>
                    )}
                </FormItem>
            </div>
        );
    }
}

const ProductCreateStep1 = Form.create()(ProductCreatePage);

export default ProductCreateStep1;