import React from 'react';
import {Table} from 'antd'
import { Link } from 'react-router-dom';
import 'whatwg-fetch'


const columns = [
  { title: '规格名', width: 100, dataIndex: 'name', key: 'name' ,fixed: 'left'},
  { title: '商品分类', width: 100, dataIndex: 'age', key: 'age'},
  { title: '排序', dataIndex: 'address', key: '1', width: 100 },
  { title: '可选项', dataIndex: 'spec', key: 'spec', width: 100 },
  { title: '代码', dataIndex: 'address', key: '2', width: 100 },
  { title: '输入方式', dataIndex: 'address', key: '3', width: 100 },
  {
    title: 'Action',
    key: 'operation',
    fixed: 'right',
    width: 100,
    render: (row,a,index) => {
    	return (
    		<Link to={`/app/brand/${row.key}`}>编辑</Link>
    		)
    }
  },
];

class SpecList extends React.Component {

	constructor(props) {
		super(props);
		if(this.props.hideAction){
			columns.pop();
		}
	}

	render () {
		const rowSelection = {
		  onChange: (selectedRowKeys, selectedRows) => {
		    console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
		    this.props.onSelectChange(selectedRowKeys, selectedRows);
		  },
		};
		return (
				<Table columns={columns} 
					   size="middle" 
					   bordered
					   rowSelection={this.props.showCheckbox && rowSelection}
					   pagination={this.props.pagination}
					   dataSource={this.props.dataSource} 
					   scroll={{x: '150%'}}
					   loading={this.props.loading}
					   onChange={this.handleTableChange} />
			)
	}
}

export default SpecList;